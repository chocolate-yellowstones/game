import pygame
from pygame.locals import *
import materials
import character
#import sys
#import math
from random import *
import forecast
import victory
import selection as sel

GRASS = 0
WATER = 1
FLOWER = 2
BRIDGE = 3
TREE = 4

TILESIZE = 100
MAPWIDTH = 15
MAPHEIGHT = 8
RESX = 1280
RESY = 720
DISPLAYSURF = pygame.Surface((TILESIZE * MAPWIDTH, TILESIZE * MAPHEIGHT))

textures = {
	GRASS : materials.getImage("grass2.png"),
	WATER : materials.getImage("water4.png"),
	FLOWER : materials.getImage("flower.png"),
	BRIDGE : materials.getImage("bridge.png"),
	TREE : materials.getImage("tree.png")
}

class characterSprite(pygame.sprite.Sprite):
    # Constructor. Pass in the color of the block,
    # and its x and y position
    def __init__(self, name, width, height, x, y):
		# Call the parent class (Sprite) constructor
	    pygame.sprite.Sprite.__init__(self)

		# Create an image of the block, and fill it with a color.
		# This could also be an image loaded from the disk.
	    self.image = materials.getImage(name)

		# Fetch the rectangle object that has the dimensions of the image
		# Update the position of this object by setting the values of rect.x and rect.y
	    self.rect = self.image.get_rect()
	    self.rect.x = x
	    self.rect.y = y

    def draw(self, surface, x, y):
	    surface.blit(self.image, [x, y])


lordBlueSprite = characterSprite('Lord2.png', 100, 100, 1400, 100)
lordBlueStats = character.Character("Ike", 20, 8, 8, 7, 6, 5, [7, 95, 0])
knightBlueSprite = characterSprite('Knight3.png', 100, 100, 1200, 300)
knightBlueStats = character.Character("Gatrie", 25, 9, 3, 10, 1, 4, [8, 90, 2])
fighterBlueSprite = characterSprite('Fighter2.png', 100, 100, 1100, 400)
fighterBlueStats = character.Character("Boyd", 28, 11, 5, 8, 0, 5, [9, 85, 1])

lordRedSprite = characterSprite('Lord2_r.png', 100, 100, 100, 100)
lordRedStats = character.Character("Roy", 20, 8, 8, 7, 6, 5, [7, 95, 0])
knightRedSprite = characterSprite('Knight2_r.png', 100, 100, 100, 300)
knightRedStats = character.Character("Gilliam", 25, 9, 3, 10, 1, 4, [8, 90, 2])
fighterRedSprite = characterSprite('Fighter2_r.png', 100, 100, 100, 400)
fighterRedStats = character.Character("Wallace", 28, 11, 5, 8, 0, 5, [9, 85, 1])

blueSprites = pygame.sprite.Group()
redSprites = pygame.sprite.Group()
blueSprites.add(lordBlueSprite, knightBlueSprite, fighterBlueSprite )
redSprites.add(lordRedSprite, knightRedSprite, fighterRedSprite)
selected = ''

def combat(attacker, defender):
		accuracyA = attacker.hitChance() - defender.dodge()
		accuracyB = defender.hitChance() - attacker.dodge()
		doubleAttack = False
		if attacker.getSpeed() >= defender.getSpeed() + 4:
			doubleAttack = True
			
		if randint(0, 100) <= accuracyA:
			damage = attacker.attack() - defender.getDefense()
			defender.hurt(damage)
		else:
			print('miss')

		if randint(0, 100) <= accuracyB and defender.getHealth() > 0:
			damage = defender.attack() - attacker.getDefense()
			attacker.hurt(damage)
		else:
			print('miss')

		if randint(0, 100) <= accuracyA and defender.getHealth() > 0 and attacker.getHealth() > 0 and doubleAttack == True:
			damage = attacker.attack() - defender.getDefense()
			defender.hurt(damage)
		else:
			print('miss')
		
		doubleAttack = False

def inRange(x1, y1, x2, y2):
	if (x2 == x1 - 100 and y2 == y1) or (x2 == x1 + 100 and y2 == y1) or (x2 == x1 and y2 == y1 - 100) or (x2 == x1 and y2 == y1 + 100):
		return True
	else:
		return False
def drawGrid(stage):
	if stage == 0:
		tilemap = [
			[GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, WATER, WATER, GRASS, GRASS, GRASS, FLOWER, FLOWER, FLOWER, GRASS],
			[GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, WATER, WATER, GRASS, GRASS, FLOWER, FLOWER, FLOWER, GRASS, GRASS],
			[GRASS, GRASS, GRASS, GRASS, GRASS, WATER, WATER, GRASS, GRASS, GRASS, GRASS, FLOWER, FLOWER, GRASS, GRASS],
			[FLOWER, GRASS, GRASS, GRASS, BRIDGE, BRIDGE, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS],
			[FLOWER, FLOWER, GRASS, GRASS, WATER, WATER, GRASS, GRASS, GRASS, FLOWER, GRASS, GRASS, GRASS, GRASS, GRASS],
			[FLOWER, GRASS, GRASS, GRASS, WATER, WATER, GRASS, FLOWER, FLOWER, FLOWER, GRASS, GRASS, GRASS, GRASS, GRASS],
			[GRASS, GRASS, GRASS, GRASS, GRASS, WATER, WATER, GRASS, FLOWER, FLOWER, FLOWER, GRASS, GRASS, GRASS, GRASS],
			[GRASS, GRASS, GRASS, GRASS, GRASS, WATER, WATER, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS],
	]
	elif stage == 1:
		tilemap = [
			[TREE, TREE, FLOWER, WATER, WATER, WATER, WATER, WATER, WATER, FLOWER, TREE, TREE, TREE, TREE, TREE],
			[TREE, GRASS, GRASS, FLOWER, WATER, WATER, WATER, WATER, FLOWER, GRASS, GRASS, GRASS, GRASS, GRASS, TREE],
			[TREE, GRASS, GRASS, GRASS, FLOWER, WATER, WATER, FLOWER, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, TREE],
			[TREE, GRASS, GRASS, GRASS, GRASS, FLOWER, FLOWER, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, TREE],
			[TREE, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, FLOWER, FLOWER, GRASS, GRASS, GRASS, GRASS, TREE],
			[TREE, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, FLOWER, WATER, WATER, FLOWER, GRASS, GRASS, GRASS, TREE],
			[TREE, GRASS, GRASS, GRASS, GRASS, GRASS, FLOWER, WATER, WATER, WATER, WATER, FLOWER, GRASS, GRASS, TREE],
			[TREE, TREE, TREE, TREE, TREE, FLOWER, WATER, WATER, WATER, WATER, WATER, WATER, FLOWER, TREE, TREE],
	]
	elif stage == 2:
		tilemap = [
			[TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE],
			[TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE, GRASS, TREE, TREE],
			[TREE, TREE, TREE, TREE, TREE, TREE, GRASS, GRASS, TREE, TREE, TREE, GRASS, FLOWER, GRASS, TREE],
			[TREE, TREE, TREE, TREE, TREE, GRASS, FLOWER, GRASS, GRASS, TREE, TREE, TREE, GRASS, TREE, TREE],
			[TREE, TREE, TREE, TREE, GRASS, FLOWER, FLOWER, FLOWER, GRASS, GRASS, TREE, TREE, TREE, TREE, TREE],
			[TREE, TREE, GRASS, TREE, TREE, GRASS, GRASS, FLOWER, GRASS, TREE, TREE, TREE, TREE, TREE, TREE],
			[TREE, GRASS, FLOWER, GRASS, TREE, TREE, GRASS, GRASS, TREE, TREE, TREE, TREE, TREE, TREE, TREE],
			[TREE, TREE, GRASS, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE],
	]
	for row in range(MAPHEIGHT):
			for column in range(MAPWIDTH):
				DISPLAYSURF.blit(textures[tilemap[row][column]], (column*TILESIZE,row*TILESIZE))
				pygame.draw.rect(DISPLAYSURF, [0, 0, 0], (column*TILESIZE,row*TILESIZE,TILESIZE,TILESIZE), 1)



def battleLoop(stage = 0, width=RESX, height=RESY):
	"""Main battle scene.  Takes a window width and height."""

	#cursor_picture = materials.getImage('Sword_sprite.png').convert_alpha()
	dPosSelector = [0, 0]
	xBLord = lordBlueSprite.rect.x
	yBLord = lordBlueSprite.rect.y
	xBKnight = knightBlueSprite.rect.x
	yBKnight = knightBlueSprite.rect.y
	xBFighter = fighterBlueSprite.rect.x
	yBFighter = fighterBlueSprite.rect.y

	xRLord = lordRedSprite.rect.x
	yRLord = lordRedSprite.rect.y
	xRKnight = knightRedSprite.rect.x
	yRKnight = knightRedSprite.rect.y
	xRFighter = fighterRedSprite.rect.x
	yRFighter = fighterRedSprite.rect.y
	
	bLordSelect = False
	bKnightSelect = False
	bFighterSelect = False

	rLordSelect = False
	rKnightSelect = False
	rFighterSelect = False

	x = 0
	y = 0
	dPosChar = [0, 0]
	RES = (width, height)
	currentPos = [0, 0]	
	win = pygame.display.set_mode([RESX, RESY])
	work = False
	flag = False
	outOfRange = False
	end= False
	running = True
	yes = False
	combatChance = False
	pygame.mouse.set_cursor((8,8),(0,0),(0,0,0,0,0,0,0,0),(0,0,0,0,0,0,0,0))
	selected = ''
	turn = 'Blue'
	victor = ''
	
	while running:
		drawGrid(stage)
		events = pygame.event.get()
		for event in events:
			if event.type == QUIT:
				running = False
			elif event.type == KEYDOWN:
				if event.key == K_UP:
					dPosSelector[1] -= TILESIZE
					if flag == True:
						dPosChar[1] -= TILESIZE
				elif event.key == K_DOWN:
					dPosSelector[1] += TILESIZE
					if flag == True:
						dPosChar[1] += TILESIZE
				elif event.key == K_LEFT:
					dPosSelector[0] -= TILESIZE
					if flag == True:
						dPosChar[0] -= TILESIZE
				elif event.key == K_RIGHT:
					dPosSelector[0] += TILESIZE
					if flag == True:
						dPosChar[0] += TILESIZE
				elif event.key == K_ESCAPE:
					return
				elif event.key == K_x:
					yes = True
				elif event.key == K_c:
					yes = False
				elif event.key == K_SPACE and flag == False:
					if currentPos == [xBLord, yBLord] and turn == 'Blue':
						selected = 'Blue Lord'
						bLordSelect = True
						flag = True
					elif currentPos == [xRLord, yRLord] and turn == 'Red':
						selected = 'Red Lord'
						rLordSelect = True
						flag = True
					elif currentPos == [xBKnight, yBKnight] and turn == 'Blue':
						bKnightSelect = True
						selected = 'Blue Knight'
						flag = True
					elif currentPos == [xRKnight, yRKnight] and turn == 'Red':
						rKnightSelect = True
						selected = 'Red Knight'
						flag = True
					elif currentPos == [xBFighter, yBFighter] and turn == 'Blue':
						bFighterSelect = True
						selected = 'Blue Fighter'
						flag = True
					elif currentPos == [xRFighter, yRFighter] and turn == 'Red':
						rFighterSelect = True
						selected = 'Red Fighter'
						flag = True
					dPosChar = [0, 0]
					
 
				elif event.key == K_SPACE and flag == True:
					if selected == 'Blue Lord':
						if abs(dPosChar[0]) + abs(dPosChar[1]) > lordBlueStats.getMovement() * 100:
							print('Out of Range')
						else:
							if not (((dPosChar[0] + xBLord) == xBKnight and (dPosChar[1] + yBLord) == yBKnight) or ((dPosChar[0] + xBLord) == xRKnight and (dPosChar[1] + yBLord) == yRKnight) or ((dPosChar[0] + xBLord) == xRLord and (dPosChar[1] + yBLord) == yRLord) or ((dPosChar[0] + xBLord) == xBFighter and (dPosChar[1] + yBLord) == yBFighter) or ((dPosChar[0] + xBLord) == xRFighter and (dPosChar[1] + yBLord) == yRFighter)):
								xBLord += dPosChar[0]
								yBLord += dPosChar[1]
								
								if inRange(xBLord, yBLord, xRLord, yRLord):
									if forecast.forecast(forecast.gameInit(), lordBlueStats, lordRedStats, turn) == True:
										combat(lordBlueStats, lordRedStats)
									end = True

								elif inRange(xBLord, yBLord, xRKnight, yRKnight):
									if forecast.forecast(forecast.gameInit(), lordBlueStats, knightRedStats, turn) == True:
										combat(lordBlueStats, knightRedStats)
									end= True

								elif inRange(xBLord, yBLord, xRFighter, yRFighter):
									if forecast.forecast(forecast.gameInit(), lordBlueStats, fighterRedStats, turn) == True:
										combat(lordBlueStats, fighterRedStats)
									end = True

								else:
									end = True

								if end == True:
									bLordSelect = False
									flag = False 
									end = False
									yes = False
									selected = ''
									turn = 'Red'

					elif selected == 'Red Lord':
						if abs(dPosChar[0]) + abs(dPosChar[1]) > lordRedStats.getMovement() * 100:
							print('Out of Range')
						else:
							if not (((dPosChar[0] + xRLord) == xBLord and (dPosChar[1] + yRLord) == yBLord) or ((dPosChar[0] + xRLord) == xBKnight and (dPosChar[1] + yRLord) == yBKnight) or ((dPosChar[0] + xRLord) == xRKnight and (dPosChar[1] + yRLord) == yRKnight )or ((dPosChar[0] + xRLord) == xBFighter and (dPosChar[1] + yRLord) == yBFighter) or ((dPosChar[0] + xRLord) == xRFighter and (dPosChar[1] + yRLord) == yRFighter)):
								xRLord += dPosChar[0]
								yRLord += dPosChar[1]

								if inRange(xRLord, yRLord, xBLord, yBLord):
									if forecast.forecast(forecast.gameInit(), lordRedStats, lordBlueStats, turn) == True:
										combat(lordRedStats, lordBlueStats)
									end = True
									
								elif inRange(xRLord, yRLord, xBKnight, yBKnight):
									if forecast.forecast(forecast.gameInit(), lordRedStats, knightBlueStats, turn) == True:
										combat(lordRedStats, knightBlueStats)
									end = True

								elif inRange(xRLord, yRLord, xBFighter, yBFighter):
									if forecast.forecast(forecast.gameInit(), lordRedStats, fighterBlueStats, turn) == True:
										combat(lordRedStats, fighterBlueStats)
									end = True
								
								else:
									end = True

								if end == True:
									rLordSelect = False
									flag = False 
									end = False
									selected = ''
									turn = 'Blue'

					elif selected == 'Blue Knight':
						if abs(dPosChar[0]) + abs(dPosChar[1]) > knightBlueStats.getMovement() * 100:
							print('Out of Range')
						else:
							if not (((dPosChar[0] + xBKnight) == xBLord and (dPosChar[1] + yBKnight) == yBLord) or ((dPosChar[0] + xBKnight) == xRLord and (dPosChar[1] + yBKnight) == yRLord) or ((dPosChar[0] + xBKnight) == xRKnight and (dPosChar[1] + yBKnight) == yRKnight) or ((dPosChar[0] + xBKnight) == xBFighter and (dPosChar[1] + yBKnight) == yBFighter) or ((dPosChar[0] + xBKnight) == xRFighter and (dPosChar[1] + yBKnight) == yRFighter)):
								xBKnight += dPosChar[0]
								yBKnight += dPosChar[1] 
								
								if inRange(xBKnight, yBKnight, xRLord, yRLord):
									if forecast.forecast(forecast.gameInit(), knightBlueStats, lordRedStats, turn) == True:
										combat(knightBlueStats, lordRedStats)
									end = True

								elif inRange(xBKnight, yBKnight, xRKnight, yRKnight):
									if forecast.forecast(forecast.gameInit(), knightBlueStats, knightRedStats, turn) == True:
										combat(knightBlueStats, knightRedStats)
									end = True

								elif inRange(xBKnight, yBKnight, xRFighter, yRFighter):
									if forecast.forecast(forecast.gameInit(), knightBlueStats, fighterRedStats, turn) == True:
										combat(knightBlueStats, fighterRedStats)
									end = True

								else:
									end = True

								if end == True:
									bKnightSelect = False
									flag = False 
									end = False
									selected = ''
									turn = 'Red'

					elif selected == 'Red Knight':
						if abs(dPosChar[0]) + abs(dPosChar[1]) > knightBlueStats.getMovement() * 100:
							print('Out of Range')
						else:
							if not (((dPosChar[0] + xRKnight) == xBLord and (dPosChar[1] + yRKnight) == yBLord) or ((dPosChar[0] + xRKnight) == xRLord and (dPosChar[1] + yRKnight) == yRLord) or ((dPosChar[0] + xRKnight) == xBKnight and (dPosChar[1] + yRKnight) == yBKnight) or ((dPosChar[0] + xRKnight) == xBFighter and (dPosChar[1] + yRKnight) == yBFighter) or ((dPosChar[0] + xRKnight) == xRFighter and (dPosChar[1] + yRKnight) == yRFighter)):
								xRKnight += dPosChar[0]
								yRKnight += dPosChar[1] 
								
								if inRange(xRKnight, yRKnight, xBLord, yBLord):
									if forecast.forecast(forecast.gameInit(), knightRedStats, lordBlueStats, turn) == True:
										combat(knightBlueStats, lordRedStats)
									end = True

								elif inRange(xRKnight, yRKnight, xBKnight, yBKnight):
									if forecast.forecast(forecast.gameInit(), knightRedStats, knightBlueStats, turn) == True:
										combat(knightRedStats, knightBlueStats)
									end = True

								elif inRange(xRKnight, yRKnight, xBFighter, yBFighter):
									if forecast.forecast(forecast.gameInit(), knightRedStats, fighterBlueStats, turn) == True:
										combat(knightRedStats, fighterBlueStats)
									end = True

								else:
									end = True

								if end == True:
									rKnightSelect = False
									flag = False 
									end = False
									selected = ''
									turn = 'Blue'
						
					elif selected == 'Blue Fighter':
						if abs(dPosChar[0]) + abs(dPosChar[1]) > fighterBlueStats.getMovement() * 100:
							print('Out of Range')
						else:
							if not (((dPosChar[0] + xBFighter) == xBLord and (dPosChar[1] + yBFighter) == yBLord) or ((dPosChar[0] + xBFighter) == xRLord and (dPosChar[1] + yBFighter) == yRLord) or ((dPosChar[0] + xBFighter) == xBKnight and (dPosChar[1] + yBFighter) == yBKnight) or ((dPosChar[0] + xBFighter) == xRKnight and (dPosChar[1] + yBFighter) == yRKnight) or ((dPosChar[0] + xBFighter) == xRFighter and (dPosChar[1] + yBFighter) == yRFighter)):
								xBFighter += dPosChar[0]
								yBFighter += dPosChar[1] 
								
								if inRange(xBFighter, yBFighter, xRLord, yRLord):
									if forecast.forecast(forecast.gameInit(), fighterBlueStats, lordRedStats, turn) == True:
										combat(fighterBlueStats, lordRedStats)
									end = True

								elif inRange(xBFighter, yBFighter, xRKnight, yRKnight):
									if forecast.forecast(forecast.gameInit(), fighterBlueStats, knightRedStats, turn) == True:
										combat(fighterBlueStats, knightRedStats)
									end = True

								elif inRange(xBFighter, yBFighter, xRFighter, yRFighter):
									if forecast.forecast(forecast.gameInit(), fighterBlueStats, fighterRedStats, turn) == True:
										combat(fighterBlueStats, fighterRedStats)
									end = True

								else:
									end = True

								if end == True:
									bFighterSelect = False
									flag = False 
									end = False
									selected = ''
									turn = 'Red'

					elif selected == 'Red Fighter':
						if abs(dPosChar[0]) + abs(dPosChar[1]) > fighterRedStats.getMovement() * 100:
							print('Out of Range')
						else:
							if not (((dPosChar[0] + xRFighter) == xBLord and (dPosChar[1] + yRFighter) == yBLord) or ((dPosChar[0] + xRFighter) == xRLord and (dPosChar[1] + yRFighter) == yRLord) or ((dPosChar[0] + xRFighter) == xBKnight and (dPosChar[1] + yRFighter) == yBKnight) or ((dPosChar[0] + xRFighter) == xRKnight and (dPosChar[1] + yRFighter) == yRKnight) or ((dPosChar[0] + xRFighter) == xBFighter and (dPosChar[1] + yRFighter) == yBFighter)):
								xRFighter += dPosChar[0]
								yRFighter += dPosChar[1] 
								
								if inRange(xRFighter, yRFighter, xBLord, yBLord):
									if forecast.forecast(forecast.gameInit(), fighterBlueStats, lordRedStats, turn) == True:
										combat(fighterRedStats, lordBlueStats)
									end = True

								elif inRange(xRFighter, yRFighter, xBKnight, yBKnight):
									if forecast.forecast(forecast.gameInit(), fighterRedStats, knightBlueStats, turn) == True:
										combat(fighterRedStats, knightBlueStats)
									end = True

								elif inRange(xRFighter, yRFighter, xBFighter, yBFighter):
									if forecast.forecast(forecast.gameInit(), fighterRedStats, fighterBlueStats, turn) == True:
										combat(fighterRedStats, fighterBlueStats)
									end = True

								else:
									end = True

								if end == True:
									rFighterSelect = False
									flag = False 
									end = False
									selected = ''
									turn = 'Blue'

				elif (event.key == K_z and flag == True):
					dPosChar[0] = 0
					dPosChar[1] = 0
					flag = False
					bKnightSelect = False
					bLordSelect = False
					bFighterSelect = False

					rLordSelect = False
					rKnightSelect = False
					rFighterSelect = False
					if turn == 'Blue':
						turn = 'Red'
					elif turn == 'Red':
						turn = 'Blue'
				# elif event.type == VIDEORESIZE:
				#	RES = event.size
				#	win = pygame.display.set_mode(RES, RESIZABLE)				
			
		
		
		currentPos[0] = dPosSelector[0] + currentPos[0]
		currentPos[1] = dPosSelector[1] + currentPos[1]
		dPosSelector = [0, 0]	
		
		if currentPos[0] <= 0:
			currentPos[0] = 0
		elif currentPos[0] >= 1400:
			currentPos[0] = 1400
	
		if currentPos[1] <= 0:
			currentPos[1] = 0
		elif currentPos[1] >= 700:
			currentPos[1] = 700

		if x <= 0:
			x = 0
		elif x >= 1400:
			x = 1400
	
		if y <= 0:
			y = 0
		elif y >= 700:
			y = 700
	

		if bLordSelect == True:
			pygame.draw.rect(DISPLAYSURF, [255,105,180], (xBLord, yBLord, TILESIZE, TILESIZE))
		elif rLordSelect == True:
			pygame.draw.rect(DISPLAYSURF, [255,105,180], (xRLord, yRLord, TILESIZE, TILESIZE))
		elif bKnightSelect == True:
			pygame.draw.rect(DISPLAYSURF, [255,105,180], (xBKnight, yBKnight, TILESIZE, TILESIZE))
		elif rKnightSelect == True:
			pygame.draw.rect(DISPLAYSURF, [255,105,180], (xRKnight, yRKnight, TILESIZE, TILESIZE))
		elif bFighterSelect == True:
			pygame.draw.rect(DISPLAYSURF, [255,105,180], (xBFighter, yBFighter, TILESIZE, TILESIZE))
		elif rFighterSelect == True:
			pygame.draw.rect(DISPLAYSURF, [255,105,180], (xRFighter, yRFighter, TILESIZE, TILESIZE))
			

		if turn == "Red":
			pygame.draw.rect(DISPLAYSURF, [255, 0, 0], (currentPos[0], currentPos[1], TILESIZE, TILESIZE), 2)
		else:
			pygame.draw.rect(DISPLAYSURF, [0, 0, 255], (currentPos[0], currentPos[1], TILESIZE, TILESIZE), 2)
		if outOfRange == False:
			if lordBlueStats.getHealth() > 0:
				lordBlueSprite.draw(DISPLAYSURF, xBLord, yBLord)
			else:
				lordBlueSprite.kill()
				xBLord = -500
				yBLord = -500

			if knightBlueStats.getHealth() > 0:
				knightBlueSprite.draw(DISPLAYSURF, xBKnight, yBKnight)
			else:
				knightBlueSprite.kill()
				xBKnight = -500
				yBKnight = -500

			if fighterBlueStats.getHealth() > 0:
				fighterBlueSprite.draw(DISPLAYSURF, xBFighter, yBFighter)
			else:
				fighterBlueSprite.kill()
				xBFighter = -500
				yBFighter = -500

			if lordRedStats.getHealth() > 0:
				lordRedSprite.draw(DISPLAYSURF, xRLord, yRLord)
			else:
				lordRedSprite.kill()
				xRLord = -500
				yRLord = -500

			if knightRedStats.getHealth() > 0:
				knightRedSprite.draw(DISPLAYSURF, xRKnight, yRKnight)
			else:
				knightRedSprite.kill()
				xRKnight = -500
				yRKnight = -500

			if fighterRedStats.getHealth() > 0:
				fighterRedSprite.draw(DISPLAYSURF, xRFighter, yRFighter)
			else:
				fighterRedSprite.kill()
				xRFighter = -500
				yRFighter = -500
				
		maindisplay = pygame.transform.scale(DISPLAYSURF, RES)			
		rect = win.blit(source=maindisplay, dest=(0, 0), area=pygame.Rect((0, 0), RES))
		# win.blit(cursor_picture, pygame.mouse.get_pos())
		pygame.display.flip()
		blueSprites.update()
		redSprites.update()
		
		if running == False:
			quit()

		if len(blueSprites) == 0:
			victor = 'Red'
			running = False
		elif len(redSprites) == 0:
			victor = 'Blue'
			running = False

	victory.victory(victory.gameInit(), victor)

if __name__ == "__main__":
	pygame.init()
	battleLoop()

