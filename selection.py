import pygame
from pygame.locals import *
import materials

stage = 0
def getStage(): 
	return stage

def selectScreen():
	from grid2 import battleLoop
	select = pygame.display.set_mode([800,600])
	pygame.display.set_caption("Selection Screen")
	selbg = pygame.image.load('selectionbg.png')
	select.blit(selbg,(0,0))
	meadow = materials.getImage("stage0.png")
	lakes = materials.getImage("stage1.png")
	forest = materials.getImage("stage2.png")
	bg = materials.getImage('Background.png')
	cp = materials.getImage('Sword_sprite.png').convert_alpha()

	stages = [meadow, lakes, forest]
	x = 0
	select.blit(stages[x], (150,100))
	

	running = True

	while running:
		
		select.blit(selbg,(0,0))
		select.blit(stages[x], (150,100))
		select.blit(cp,(pygame.mouse.get_pos()))
		def square():
			select.blit(selbg,(0,0))

		lastclickx = 0
		lastclicky = 0

		events = pygame.event.get()
		for event in events:
			if event.type == QUIT:
				running = False
			if event.type == MOUSEBUTTONDOWN:
				lastclickx, lastclicky = event.pos
				next = True
				back = True
				if lastclickx >= (550) and lastclicky >= (400) and lastclickx <= (700) and lastclicky <= (500):
					while next:
						square()
						x = x + 1
						if x > 2:
							x = 0
						select.blit(stages[x], (150,100))
						next = False
						stage = x
				if lastclickx >= (100) and lastclicky >= (250) and lastclickx <= (400) and lastclicky <= (500):
					while back:
						square()
						x = x - 1
						if x < 0:
							x = 2
						select.blit(stages[x], (150,100))
						back = False
						stage = x
				if lastclickx >= (325) and lastclicky >= (400) and lastclickx <= (475) and lastclicky <= (500):
						battleLoop(x)
				
		pygame.display.update()

if __name__ == "__main__":
	selectScreen()

