class Character:
	def __init__(self, name, health, strength, speed, defense, resistance, movement, weapon):
		self.name = name
		self.health = health
		self.strength = strength
		self.speed = speed
		self.defense = defense
		self.resistance = resistance
		self.movement = movement
		self.weapon = weapon #Weapon is an array; 0 is Might, 1 is Hit Chance, 2 is Weapon type
		#Weapon types: 0 - swords, 1 - axe, 2 - lance

	def __str__(self):
		return str(self.name)
	def getName(self):
		return self.name
	def setName(self):
		self.name = name
	def getHealth(self):
		return self.health
	def hurt(self, amount):
		if amount > 0:
			self.health -= amount
	def heal(self, amount):
		if amount > 0:
			self.health += amount
	def getStrength(self):
		return self.strength
	def setStrength(self, amount):
		self.strength = amount
	def getSpeed(self):
		return self.speed
	def setSpeed(self, amount):
		self.speed = speed
	def getDefense(self):
		return self.defense
	def setDefense(self, amount):
		self.defense = amount
	def getResistance(self):
		return self.resistance
	def setResistance(self, amount):
		self.resistance = amount
	def getMovement(self):
		return self.movement
	def setMovement(self, amount):
		self.movement = amount
	def getWeapon(self):
		return self.weapon
	def setWeapon(self, weapon):
		self.weapon = weapon
	def hitChance(self):
		return self.weapon[1]
	def dodge(self):
		return self.speed * 2
	def attack(self):
		return self.strength + self.weapon[0]
sword = [6, .90, "Iron Sword"]
lance = [7, .85, "Iron Lance"]
stave = [6, .90, "Wooden Staff"]
tome = [5, .90, "Fire Tome"]
axe = [8, .80, "Iron Axe"]

lord = 		Character("Lord",	20, 8, 8, 9, 6, 5, sword)
knight = 	Character("Knight",	25, 9, 3, 10, 1, 4, lance)
priest = 	Character("Priest",	16, 15, 3, 2, 12, 5, stave)
mage = 		Character("Mage",	17, 1, 8, 6, 4, 5, tome)
fighter = 	Character("Fighter",	28, 11, 0, 5, 8, 5, axe)

