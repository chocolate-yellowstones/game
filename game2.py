import pygame
from pygame.locals import *
import selection
import materials	
pygame.mixer.init()
pygame.mixer.music.load('Song1.mp3')
pygame.mixer.music.play()
def gameInit():
	menu = pygame.display.set_mode([800,600])
	menu.fill((255,255,255))
	pygame.display.set_caption("Game Testing")
	return menu

def eventLoop(menu):
	font = pygame.font.Font(None, 36)
	text1 = font.render("Start", 1, (10,10,10))
	text2 = font.render("Exit", 1, (10,10,10))
	pygame.mouse.set_cursor((8,8),(0,0),(0,0,0,0,0,0,0,0),(0,0,0,0,0,0,0,0))
	cursor_picture = materials.getImage('Sword_sprite.png').convert_alpha()
	bg = materials.getImage('Background.png')
	startcolor = [255,105,180]
	while True:
		pygame.draw.rect(menu, startcolor, (300, 300, 200, 100),0)
		pygame.draw.rect(menu, [0,0,0], (300, 300, 200, 100),1)
		pygame.draw.rect(menu, startcolor, (300, 150, 200, 100),0)
		pygame.draw.rect(menu, [0,0,0], (300, 150, 200, 100),1)
		pygame.draw.rect(menu, [0,0,0], (100, 450, 600, 100),1)
		lastclickx = 0
		lastclicky = 0
		for event in pygame.event.get():
			if event.type==QUIT:
				return
			if event.type == MOUSEBUTTONDOWN:
				lastclickx, lastclicky = event.pos
				if lastclickx >= (300) and lastclicky >= (300) and lastclickx <= (500) and lastclicky <= (400):
					exit()
				if lastclickx >= (300) and lastclicky >= (150) and lastclickx <= (500) and lastclicky <= (250):
					selection.selectScreen()
					menu = gameInit()
			if event.type == MOUSEMOTION:
				posx, posy = event.pos
				if posx >= (300) and posy >= (300) and posx <= (500) and posy <= (400):
					text2 = font.render("Exit", 1, (255,255,255))
				else:
					text2 = font.render("Exit", 1, (0,0,0))
				if posx >= (300) and posy >= (150) and posx <= (500) and posy <= (250):
					text1 = font.render("Start", 1, (255,255,255))
				else:
					text1 = font.render("Start", 1, (0,0,0))
		menu.blit(bg, (0,0))
		pygame.draw.rect(menu, startcolor, (300, 300, 200, 100),0)
		pygame.draw.rect(menu, [0,0,0], (300, 300, 200, 100),1)
		pygame.draw.rect(menu, startcolor, (300, 150, 200, 100),0)
		pygame.draw.rect(menu, [0,0,0], (300, 150, 200, 100),1)
		menu.blit(text2, (370, 330))
		menu.blit(text1, (375, 180))
		menu.blit(cursor_picture, pygame.mouse.get_pos())
		pygame.display.flip()

if __name__ == "__main__":
	pygame.init()
	eventLoop(gameInit())
	pygame.quit()
