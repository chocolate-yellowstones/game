import pygame, sys
from pygame.locals import *

TILESIZE = 100
MAPWIDTH = 15
MAPHEIGHT = 8
RESX = 1280
RESY = 720
DISPLAYSURF = pygame.Surface((TILESIZE * MAPWIDTH, TILESIZE * MAPHEIGHT))

GRASS = 0
WATER = 1
FLOWER = 2
BRIDGE = 3

textures = {
	GRASS : pygame.image.load("game/grass2.png"),
	WATER : pygame.image.load("game/water4.png"),
    FLOWER : pygame.image.load("game/flower.png"),
    BRIDGE : pygame.image.load("game/bridge.png"),
}

def plains():
    tilemap = [
        [GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, WATER, WATER, GRASS, GRASS, GRASS, FLOWER, FLOWER, FLOWER, GRASS],
        [GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, WATER, WATER, GRASS, GRASS, FLOWER, FLOWER, FLOWER, GRASS, GRASS],
        [GRASS, GRASS, GRASS, GRASS, GRASS, WATER, WATER, GRASS, GRASS, GRASS, GRASS, FLOWER, FLOWER, GRASS, GRASS],
        [FLOWER, GRASS, GRASS, GRASS, BRIDGE, BRIDGE, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS],
        [FLOWER, FLOWER, GRASS, GRASS, WATER, WATER, GRASS, GRASS, GRASS, FLOWER, GRASS, GRASS, GRASS, GRASS, GRASS],
        [FLOWER, GRASS, GRASS, GRASS, WATER, WATER, GRASS, FLOWER, FLOWER, FLOWER, GRASS, GRASS, GRASS, GRASS, GRASS],
        [GRASS, GRASS, GRASS, GRASS, GRASS, WATER, WATER, GRASS, FLOWER, FLOWER, GRASS, GRASS, GRASS, GRASS, GRASS],
        [GRASS, GRASS, GRASS, GRASS, GRASS, WATER, WATER, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS, GRASS],
    ]

# def drawGrid():
#     for row in range(MAPHEIGHT):
#         for column in range(MAPWIDTH):
#             DISPLAYSURF.blit(textures[tilemap[row][column]], (column*TILESIZE,row*TILESIZE))
#             pygame.draw.rect(DISPLAYSURF, [0, 0, 0], (column*TILESIZE,row*TILESIZE,TILESIZE,TILESIZE), 1)