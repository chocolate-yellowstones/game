import pygame
from pygame.locals import *
import grid2
import materials
import character

def gameInit():
	victory = pygame.display.set_mode([1280, 720])
	victory.fill((255,255,255))
	pygame.display.set_caption("Victory")
	return victory

def victory(victory, team):
	while True:
		if team == 'Red':
			message = materials.getImage('VictoryRed.png')

		elif team == 'Blue':
			message = materials.getImage('VictoryBlue.png')

		victory.blit(message, (-100, 0))
		for event in pygame.event.get():
			if event.type == QUIT:
				return

		pygame.display.flip()

pygame.init()
victory(gameInit(), 'Blue')