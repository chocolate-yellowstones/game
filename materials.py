from pygame import image, Surface

cache = []

class Image:
	"""Base Image class - handles importing images. Do not call this class directly!!!"""
	def __init__(self, name, **kwds):
		self.name = name
		self.surface = image.load(name)
		
	def __str__(self):
		return self.name

def getImage(name):
	"""Import images with caching"""
	for x in cache:
		if str(x) == name:
			return x.surface
	#we've iterated through the entire list and haven't found our image already in use,
	#so we can create a new object to use
	image = Image(name)
	cache.append(image)
	return image.surface
