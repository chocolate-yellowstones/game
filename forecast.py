import pygame
from pygame.locals import *
import grid2
import materials
import character

def gameInit():
	forecast = pygame.display.set_mode([1280, 720])
	forecast.fill((255,255,255))
	pygame.display.set_caption("Forecast")
	return forecast

def forecast(forecast, attacker, defender, turn):
	sword = materials.getImage('Sword_sprite.png').convert_alpha()
	font = pygame.font.Font(None, 36)
	attack = font.render('Accept', 1, [0, 0, 0])
	cancel = font.render('Cancel', 1, [0, 0, 0])
	hA = attacker.getHealth()
	hD = defender.getHealth()
	dA = attacker.attack() - defender.getDefense()
	dD = defender.attack() - attacker.getDefense()
	accA = attacker.hitChance() - defender.dodge()
	accD = defender.hitChance() - attacker.dodge()
	healthA = font.render('Health: ' + str(hA), 1, [0, 0, 0])
	healthD = font.render('Health: ' + str(hD), 1, [0, 0, 0])
	if attacker.getSpeed() > defender.getSpeed() + 4:
		dmgA = font.render('Attack: ' + str(dA) + ' x2', 1, [0, 0, 0])
		dmgD = font.render('Attack: ' + str(dD), 1, [0, 0, 0])
	elif defender.getSpeed() > attacker.getSpeed() + 4:
		dmgA = font.render('Attack: ' + str(dA), 1, [0, 0, 0])
		dmgD = font.render('Attack: ' + str(dD) + ' x2', 1, [0, 0, 0])
	else:
		dmgA = font.render('Attack: ' + str(dA), 1, [0, 0, 0])
		dmgD = font.render('Attack: ' + str(dD), 1, [0, 0, 0])
	
	accuracyA = font.render('Hit Chance: ' + str(accA), 1, [0, 0, 0])
	accuracyD = font.render('Hit Chance: ' + str(accD), 1, [0, 0, 0])


	while True:
		xClick = 0
		yClick = 0
		for event in pygame.event.get():
			if event.type == QUIT:
				return
			if event.type == MOUSEBUTTONDOWN:
				xClick, yClick = event.pos
				if xClick <= 640 and xClick >= 440 and yClick <= 480 and yClick >= 380:
					print('true')
					return True
				elif xClick <= 840 and xClick >= 640 and yClick <= 480 and yClick >= 380:
					print('false')
					return False

		if turn == 'Blue':
			pygame.draw.rect(forecast, [0, 191, 255], (0, 0, 640, 720))
			pygame.draw.rect(forecast, [178, 34, 34], (640, 0, 640, 720))
		elif turn == 'Red':
			pygame.draw.rect(forecast, [255, 0, 0], (0, 0, 640, 720))
			pygame.draw.rect(forecast, [0, 191, 255], (640, 0, 640, 720))

		pygame.draw.rect(forecast, [0, 0, 0], (440, 380, 200, 100), 2)
		pygame.draw.rect(forecast, [0, 0, 0], (640, 380, 200, 100), 2)
		forecast.blit(attack, (490, 420))
		forecast.blit(cancel, (690, 420))
		forecast.blit(healthA, (460, 130))
		forecast.blit(healthD, (660, 130))
		forecast.blit(dmgA, (460, 230))
		forecast.blit(dmgD, (660, 230))
		forecast.blit(accuracyA, (460, 330))
		forecast.blit(accuracyD, (660, 330))
		forecast.blit(sword, pygame.mouse.get_pos())
		pygame.display.flip()


# pygame.init()
# lordBlueStats = character.Character("Ike", 20, 8, 8, 7, 6, 5, [7, 90, 0])
# lordRedStats = character.Character("Roy", 20, 8, 8, 7, 6, 5, [7, 90, 0])
# forecast(gameInit(), lordRedStats, lordBlueStats, 'Red')
